<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeRequest;
use App\Http\Resources\EmployeeResource;
use App\Models\Employee;

class EmployeeController extends Controller
{
    public function index()
    {
        
    }

    public function store(EmployeeRequest $request)
    {
        if(isset($request->status) && $request->status == 'add'){
            $Employee = Employee::create([
                'firstname' => $request->firstname,
                'lastname' => $request->lastname,
                'email' => $request->email,
                'companies_id' => $request->id,
                'phone' => $request->phone
            ]);

            $employees = $request->id;
        } else {

            $Employee = Employee::where('id', $request->id)->first();
            $Employee->firstname = $request->firstname;
            $Employee->lastname = $request->lastname;
            $Employee->email = $request->email;
            $Employee->phone = $request->phone;
            $Employee->save();

            $employees = $Employee->companies_id;
        }


        return $employees;
    }

    public function show($id)
    {
        $employees = Employee::with(['Company'])
                                ->where('companies_id', $id)
                                ->get();
        return EmployeeResource::collection($employees);
    }

    public function update(EmployeeRequest $request, Employee $Employee)
    {
        $Employee->update($request->validated());

        return new EmployeeResource($Employee);
    }

    public function destroy(Employee $employee)
    {
        $employee->delete();

        return response()->noContent();
    }
}
