<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'email' => ['required', 'email'],
            'logo' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            'website' => ['nullable', 'url'],
        ];
    }
}
