<?php

// CheckUserEmailMiddleware.php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckUserEmail
{
    public function handle(Request $request, Closure $next)
    {
        $user = Auth::user();
       
        if ($user && $user->email === 'user@grtech.com ') {
            return response()->json(['error' => 'Unauthorized'], 403);
        }

        return $next($request);
    }
}
