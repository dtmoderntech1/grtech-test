import { createRouter, createWebHistory } from "vue-router";

import CompaniesIndex from '@/components/companies/CompaniesIndex.vue'
import CompaniesCreate from '@/components/companies/CompaniesCreate.vue'
import CompaniesEdit from '@/components/companies/CompaniesEdit.vue'

import EmployeesIndex from '@/components/employees/EmployeesIndex.vue'
import EmployeesCreate from '@/components/employees/EmployeesCreate.vue'

const routes = [
    {
        path: '/dashboard',
        name: 'companies.index',
        component: CompaniesIndex
    },
    {
        path: '/companies/create',
        name: 'companies.create',
        component: CompaniesCreate
    },
    {
        path: '/companies/:id/edit',
        name: 'companies.edit',
        component: CompaniesEdit,
        props: true
    },
    {
        path: '/employees/:id',
        name: 'employees',
        component: EmployeesIndex,
        props: true
    },
    {
        path: '/employees/:id/edit',
        name: 'employees.edit',
        component: EmployeesIndex,
        props: true
    },
    {
        path: '/employees/create',
        name: 'employees.create',
        component: EmployeesCreate
    },
]

export default createRouter({
    history: createWebHistory(),
    routes
})
